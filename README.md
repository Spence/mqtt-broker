# MQTT Broker

An **experimental** MQTT broker on top of [Netty](netty.io).

## DEPRECATED

This project has been rolled into [Azondi](https://github.com/opensensorsio/azondi/) and is now **deprecated**.

## License

Copyright © 2014 opensensors.io

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.

The use and distribution terms for this software are covered by the [Eclipse Public License 1.0](http://opensource.org/licenses/eclipse-1.0.php) which can be found in the file epl-v10.html at the root of this distribution. By using this software in any fashion, you are agreeing to be bound by the terms of this license. You must not remove this notice, or any other, from this software.
